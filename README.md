<!--
 Copyright (c) 2019 RoccoDev
 
 This software is released under the MIT License.
 https://opensource.org/licenses/MIT
-->

# Wii hidden channels loader
This homebrew app lets you load your Wii's hidden channels.