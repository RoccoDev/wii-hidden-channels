// Copyright (c) 2019 RoccoDev
//
// This software is released under the MIT License.
// https://opensource.org/licenses/MIT

#include <stdio.h>
#include <stdlib.h>
#include <gccore.h>
#include <wiiuse/wpad.h>

#define TITLE_ID(x, y) (((u64)(x) << 32) | (y))

static void *xfb = nullptr;
static GXRModeObj *rmode = nullptr;

void initVideo()
{
	// Initialise the video system
	VIDEO_Init();

	// This function initialises the attached controllers
	WPAD_Init();

	// Obtain the preferred video mode from the system
	// This will correspond to the settings in the Wii menu
	rmode = VIDEO_GetPreferredMode(nullptr);

	// Allocate memory for the display in the uncached region
	xfb = MEM_K0_TO_K1(SYS_AllocateFramebuffer(rmode));

	// Initialise the console, required for printf
	console_init(xfb, 20, 20, rmode->fbWidth, rmode->xfbHeight, rmode->fbWidth * VI_DISPLAY_PIX_SZ);

	// Set up the video registers with the chosen mode
	VIDEO_Configure(rmode);

	// Tell the video hardware where our display memory is
	VIDEO_SetNextFramebuffer(xfb);

	// Make the display visible
	VIDEO_SetBlack(FALSE);

	// Flush the video register changes to the hardware
	VIDEO_Flush();

	// Wait for Video setup to complete
	VIDEO_WaitVSync();
	if (rmode->viTVMode & VI_NON_INTERLACE)
		VIDEO_WaitVSync();

	printf("\x1b[2;0H");

	printf("Press A to open the Region Selector.\n");
	printf("Press B to open the EULA menu.\n");
}

void runAppThread()
{

	while (1)
	{
		WPAD_ScanPads();

		u32 pressed = WPAD_ButtonsDown(0);

		if (pressed & WPAD_BUTTON_A)
		{
			WII_LaunchTitle(TITLE_ID(0x00010008, 0x48414c50)); // Europe
			WII_LaunchTitle(TITLE_ID(0x00010008, 0x48414c45)); // NA
			WII_LaunchTitle(TITLE_ID(0x00010008, 0x48414c4A)); // Japan
			WII_LaunchTitle(TITLE_ID(0x00010008, 0x48414c4B)); // Korea
		}
		else if (pressed & WPAD_BUTTON_B)
		{
			WII_LaunchTitle(TITLE_ID(0x00010008, 0x48414b50)); // Europe
			WII_LaunchTitle(TITLE_ID(0x00010008, 0x48414b45)); // NA
			WII_LaunchTitle(TITLE_ID(0x00010008, 0x48414b4A)); // Japan
			WII_LaunchTitle(TITLE_ID(0x00010008, 0x48414b4B)); // Korea
		}

		VIDEO_WaitVSync();
	}
}

int main(int argc, char **argv)
{
	initVideo();
	runAppThread();
}
